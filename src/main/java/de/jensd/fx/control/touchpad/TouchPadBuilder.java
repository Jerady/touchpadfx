/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.control.touchpad;

import java.util.HashMap;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.ControlBuilder;
import javafx.util.Builder;

/**
 *
 * @author Jens
 */
public class TouchPadBuilder<B extends TouchPadBuilder<B>> extends ControlBuilder<B> implements Builder<TouchPad> {

  private HashMap<String, Property> properties = new HashMap<>();
  private static final String PREF_WIDTH = "prefWidth";
  private static final String PREF_HEIGHT = "prefHeight";
  private static final String LAYOUT_X = "layoutX";
  private static final String LAYOUT_Y = "layoutY";

  private TouchPadBuilder() {
  }

  public static TouchPadBuilder create() {
    return new TouchPadBuilder();
  }

  @Override
  public final B prefWidth(final double VALUE) {
    properties.
            put(PREF_WIDTH, new SimpleDoubleProperty(VALUE));
    return (B) this;
  }

  @Override
  public final B prefHeight(final double VALUE) {
    properties.put(PREF_HEIGHT, new SimpleDoubleProperty(VALUE));
    return (B) this;
  }

  @Override
  public final B layoutX(final double VALUE) {
    properties.put(LAYOUT_X, new SimpleDoubleProperty(VALUE));
    return (B) this;
  }

  @Override
  public final B layoutY(final double VALUE) {
    properties.put(LAYOUT_X, new SimpleDoubleProperty(VALUE));
    return (B) this;
  }

  @Override
  public TouchPad build() {
    final TouchPad CONTROL = new TouchPad();

    for (String key : properties.keySet()) {
      if (PREF_WIDTH.equals(key)) {
        CONTROL.setPrefWidth(((DoubleProperty) properties.get(key)).
                get());
      } else if (PREF_HEIGHT.equals(key)) {
        CONTROL.setPrefHeight(((DoubleProperty) properties.get(key)).
                get());
      } else if (LAYOUT_X.equals(key)) {
        CONTROL.setLayoutX(((DoubleProperty) properties.get(key)).
                get());
      } else if (LAYOUT_Y.equals(key)) {
        CONTROL.setLayoutY(((DoubleProperty) properties.get(key)).
                get());
      }
    }

    return CONTROL;

  }
}
